package com.afs.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class MarsRoverTest {
    @Test
    void should_only_plus_1_location_y_when_executeCommand_given_location_direction_north_move() {
        // given
        Location location = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.MoveForward);

        // then
        Assertions.assertEquals(new Location(0, 1, Direction.North), marsRover.getLocation());
    }

    @Test
    void should_change_direction_to_west_when_executeCommand_given_location_direction_north_turn_left() {
        // given
        Location location = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(location);


        // when
        marsRover.executeCommand(Command.TurnLeft);

        // then
        Assertions.assertEquals(new Location(0, 0, Direction.West), marsRover.getLocation());
    }

    @Test
    void should_change_direction_to_east_when_executeCommand_given_location_direction_north_turn_right() {
        // given
        Location location = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.TurnRight);

        // then
        Assertions.assertEquals(new Location(0, 0, Direction.East), marsRover.getLocation());
    }

    @Test
    void should_only_sub_1_location_y_when_executeCommand_given_location_direction_south_move() {
        // given
        Location location = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.MoveForward);

        // then
        Assertions.assertEquals(new Location(0, -1, Direction.South), marsRover.getLocation());
    }

    @Test
    void should_change_direction_to_east_when_executeCommand_given_location_direction_south_turn_left() {
        // given
        Location location = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(location);


        // when
        marsRover.executeCommand(Command.TurnLeft);

        // then
        Assertions.assertEquals(new Location(0, 0, Direction.East), marsRover.getLocation());
    }

    @Test
    void should_change_direction_to_west_when_executeCommand_given_location_direction_south_turn_right() {
        // given
        Location location = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.TurnRight);

        // then
        Assertions.assertEquals(new Location(0, 0, Direction.West), marsRover.getLocation());
    }

    @Test
    void should_only_sub_1_location_x_when_executeCommand_given_location_direction_west_move() {
        // given
        Location location = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.MoveForward);

        // then
        Assertions.assertEquals(new Location(-1, 0, Direction.West), marsRover.getLocation());
    }

    @Test
    void should_change_direction_to_south_when_executeCommand_given_location_direction_west_turn_left() {
        // given
        Location location = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(location);


        // when
        marsRover.executeCommand(Command.TurnLeft);

        // then
        Assertions.assertEquals(new Location(0, 0, Direction.South), marsRover.getLocation());
    }

    @Test
    void should_change_direction_to_north_when_executeCommand_given_location_direction_west_turn_right() {
        // given
        Location location = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.TurnRight);

        // then
        Assertions.assertEquals(new Location(0, 0, Direction.North), marsRover.getLocation());
    }

    @Test
    void should_only_plus_1_location_x_when_executeCommand_given_location_direction_east_move() {
        // given
        Location location = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.MoveForward);

        // then
        Assertions.assertEquals(new Location(1, 0, Direction.East), marsRover.getLocation());
    }

    @Test
    void should_change_direction_to_north_when_executeCommand_given_location_direction_west_turn_left() {
        // given
        Location location = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(location);


        // when
        marsRover.executeCommand(Command.TurnLeft);

        // then
        Assertions.assertEquals(new Location(0, 0, Direction.North), marsRover.getLocation());
    }

    @Test
    void should_change_direction_to_south_when_executeCommand_given_location_direction_east_turn_right() {
        // given
        Location location = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.TurnRight);

        // then
        Assertions.assertEquals(new Location(0, 0, Direction.South), marsRover.getLocation());
    }

    @Test
    void should_change_location_when_executeCommand_given_multiple_command() {
        // given
        Location location = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.MoveForward);
        marsRover.executeCommand(Command.TurnRight);
        marsRover.executeCommand(Command.MoveForward);
        marsRover.executeCommand(Command.TurnLeft);
        marsRover.executeCommand(Command.MoveForward);

        // then
        Assertions.assertEquals(new Location(1, 2, Direction.North), marsRover.getLocation());
    }

    @Test
    void should_only_sub_1_location_y_when_executeCommand_given_location_direction_north_move() {
        // given
        Location location = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.MoveBackward);

        // then
        Assertions.assertEquals(new Location(0, -1, Direction.North), marsRover.getLocation());
    }

    @Test
    void should_only_plus_1_location_y_when_executeCommand_given_location_direction_south_move() {
        // given
        Location location = new Location(0, 0, Direction.South);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.MoveBackward);

        // then
        Assertions.assertEquals(new Location(0, 1, Direction.South), marsRover.getLocation());
    }

    @Test
    void should_only_plus_1_location_y_when_executeCommand_given_location_direction_west_move() {
        // given
        Location location = new Location(0, 0, Direction.West);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.MoveBackward);

        // then
        Assertions.assertEquals(new Location(1, 0, Direction.West), marsRover.getLocation());
    }

    @Test
    void should_only_sub_1_location_y_when_executeCommand_given_location_direction_east_move() {
        // given
        Location location = new Location(0, 0, Direction.East);
        MarsRover marsRover = new MarsRover(location);

        // when
        marsRover.executeCommand(Command.MoveBackward);

        // then
        Assertions.assertEquals(new Location(-1, 0, Direction.East), marsRover.getLocation());
    }

    @Test
    void should_return_last_location_when_executeCommand_given_multiple_commands_by_command_pattern_impl() {
        // given
        Location location = new Location(0, 0, Direction.North);
        MarsRover marsRover = new MarsRover(location);
        marsRover.addCommand(new MoveForwardCommand(marsRover));
        marsRover.addCommand(new MoveForwardCommand(marsRover));
        marsRover.addCommand(new TurnRightCommand(marsRover));
        marsRover.addCommand(new MoveForwardCommand(marsRover));
        marsRover.addCommand(new MoveForwardCommand(marsRover));
        marsRover.addCommand(new TurnRightCommand(marsRover));
        marsRover.addCommand(new MoveForwardCommand(marsRover));
        marsRover.addCommand(new TurnRightCommand(marsRover));
        marsRover.addCommand(new TurnRightCommand(marsRover));
        marsRover.addCommand(new MoveForwardCommand(marsRover));
        marsRover.addCommand(new MoveBackwardCommand(marsRover));

        // when
        marsRover.executeCommands();

        // then
        Assertions.assertEquals(new Location(2, 1, Direction.North), marsRover.getLocation());
    }
}
